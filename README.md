# Title

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/project-template/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/project-template/commits/master)

## Programming Languages
![language](https://img.shields.io/badge/Q%23-3.1-blue)

## Libraries & Frameworks
| Libraries | Frameworks |
|------|------|
| | .NET Core 3.1 |

## Supported platforms
| Platform | Support |
|------|---------|
| Windows | Yes |
| Linux | Yes |
| macOS | Yes |
| Android | No |
| iOS | No |
| Bare Metal | No |